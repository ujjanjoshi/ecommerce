import { Controller, Get, Post,Headers, Body, Patch, Param, Delete, UseGuards,Request} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'src/auth/auth.service';
import { CONSTANTS } from 'src/role/constants';
import { RoleGuard } from 'src/role/roleguard/role.guard';

@Controller('user')
export class LoginController {
  constructor(
    private readonly authService:AuthService
    ) {}

  @Post('/login')
  @UseGuards(AuthGuard("local"))
  create(@Request() req) {
    // this.loginService.findOne(req.user.id);
    const token=this.authService.generateToken(req.user);
    return token;
  }

  @Get('/profile')
  @UseGuards(AuthGuard("jwt"),new RoleGuard(CONSTANTS.ROLES.ADMIN))
  public async put(@Request() req) {
   return req.user.data;
}
}
