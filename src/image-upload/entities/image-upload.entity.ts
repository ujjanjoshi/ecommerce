import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ImageUpload {
    @PrimaryGeneratedColumn()
    public id:number;

    @Column()
    public image_name:string;

    @Column({type:"timestamp", default: () => "CURRENT_TIMESTAMP"})
    public createdAt:Date

    @Column({type:"timestamp",default: () => "CURRENT_TIMESTAMP"})
    public updatedAt:Date

    @DeleteDateColumn({type:"timestamp"})
    public delete_date:Date;

}
