import { Module } from '@nestjs/common';
import { ImageUploadService } from './image-upload.service';
import { ImageUploadController } from './image-upload.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImageUpload } from './entities/image-upload.entity';

@Module({
  imports:[TypeOrmModule.forFeature([ImageUpload])],
  controllers: [ImageUploadController],
  providers: [ImageUploadService],
  exports:[ImageUploadService]
})
export class ImageUploadModule {}
