import { Injectable } from '@nestjs/common';
import { CreateImageUploadDto } from './dto/create-image-upload.dto';
import { UpdateImageUploadDto } from './dto/update-image-upload.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ImageUpload } from './entities/image-upload.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ImageUploadService {
  constructor(
    @InjectRepository(ImageUpload)
    public ImageUploadRepository:Repository<ImageUpload>
  ){}
  create(filename:string) {
    const ImageUpload=new CreateImageUploadDto();
    ImageUpload.image_name=filename;
    return this.ImageUploadRepository.save(ImageUpload);
  }

  findAll() {
    return this.ImageUploadRepository.find();
  }

  findOne(id: number) {
    return this.ImageUploadRepository.findOneBy({id:id});
  }

  update(id: number, filename:string) {
    const ImageUploadUpdate=new UpdateImageUploadDto();
    ImageUploadUpdate.image_name=filename;
    return this.ImageUploadRepository.update(id,ImageUploadUpdate);
  }

  remove(id: number) {
    return this.ImageUploadRepository.softDelete(id);
  }
}
