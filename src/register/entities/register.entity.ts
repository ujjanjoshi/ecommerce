import { Entity, PrimaryGeneratedColumn, Column, DeleteDateColumn } from "typeorm"

@Entity()
export class Register {
    @PrimaryGeneratedColumn()
    public id:number

    @Column()
    public name:string

    @Column()
    public email:string
    
    @Column()
    public role:string
    
    @Column()
    public password:string

    @Column()
    public phonenumber:string

    @DeleteDateColumn()
    public delete_at:Date
    

}
