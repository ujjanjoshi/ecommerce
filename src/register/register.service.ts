import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateRegisterDto } from './dto/create-register.dto';
import { UpdateRegisterDto } from './dto/update-register.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Register } from './entities/register.entity';

@Injectable()
export class RegisterService {
  public bcrypt = require('bcrypt');
  constructor(
    @InjectRepository(Register)
    
    public RegisterRepository:Repository<Register>
  ){}

  async create(createRegisterDto: CreateRegisterDto) {
    const password=await this.hashpassword(createRegisterDto.password);
    createRegisterDto.password=password;  
    const user=this.RegisterRepository.create(createRegisterDto);
    return this.RegisterRepository.save(user);
  }

  findAll() {
     return this.RegisterRepository.find();
  }

  async getbyemail(email:string){
    return await this.RegisterRepository.find({where:{email:email}});
  }

  async hashpassword(password:string){
    const hash = await this.bcrypt.hash(password, 10);
    return hash;
  }

  async checkpassword(email:string,password:string){
    const user=await this.getbyemail(email);
    const isPasswordMatching = await this.bcrypt.compare(password,  user[0].password);
    if(isPasswordMatching){
      return user[0];
    }
    else{
      throw new UnauthorizedException();
    }
    
  }
  findOne(id: number) {
   return this.RegisterRepository.findOneBy({id:id});
  }

  update(id: number, updateRegisterDto: UpdateRegisterDto) {
     const updateuser=this.RegisterRepository.update(id,updateRegisterDto);
    if(updateuser){
      return this.RegisterRepository.findOneBy({id:id});
    }
  }

  remove(id: number) {
    return this.RegisterRepository.softDelete(id);
  }
}
