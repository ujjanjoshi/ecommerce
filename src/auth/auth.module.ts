import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { RegisterModule } from "src/register/register.module";
import { LocalStrategy } from "./local.strategy";
import { JwtModule } from "@nestjs/jwt";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "./jwt.strategy";

@Module({
    imports:[PassportModule,RegisterModule,JwtModule.register({
        secret:"key",
        // signOptions:{expiresIn:"60s"}
    })],
    providers: [LocalStrategy,AuthService,JwtStrategy],
    exports:[AuthService]
  })
  export class AuthModule {}