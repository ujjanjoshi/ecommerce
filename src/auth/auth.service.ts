import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { Register } from "src/register/entities/register.entity";

@Injectable()
export class AuthService{
    constructor(private readonly jwtService:JwtService){}

    generateToken(payload:Register):object{
        const data={
            id:payload.id,
            name:payload.name,
            email:payload.email,
            role:payload.role,
            phonenumber:payload.phonenumber
        }

        const token= this.jwtService.sign({data});
        return {
            token:token,
            userdetails:data
        }
    }
}