import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import {Strategy} from 'passport-local';
import { Register } from "src/register/entities/register.entity";
import { RegisterService } from "src/register/register.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy){
    constructor(private readonly userService:RegisterService ){
        super();
    }
    async validate(username:string,password:string):Promise<Register>{
        const user:Register=await this.userService.checkpassword(username,password);
        return user;
        // if(user===undefined){
        //     throw new UnauthorizedException();
        // }
        // if(user!= undefined){
        //     return user;
        // }else{
        //     throw new UnauthorizedException();
        // }
    }
}