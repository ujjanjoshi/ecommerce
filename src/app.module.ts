import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { CategoryModule } from './category/category.module';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './database/database.module';
import { RegisterModule } from './register/register.module';
import { AuthModule } from './auth/auth.module';
import { LoginModule } from './login/login.module';
import { ImageUploadModule } from './image-upload/image-upload.module';
import { ProductCartModule } from './product-cart/product-cart.module';
import { CheckoutModule } from './checkout/checkout.module';

@Module({
  imports: [ProductModule,CategoryModule,DatabaseModule,CheckoutModule,ProductCartModule, ConfigModule.forRoot({
    validationSchema: Joi.object({
      POSTGRES_HOST: Joi.string().required(),
      POSTGRES_PORT: Joi.number().required(),
      POSTGRES_USER: Joi.string().required(),
      POSTGRES_PASSWORD: Joi.string().required(),
      POSTGRES_DB: Joi.string().required(),
      PORT: Joi.number(),
    })
  }), RegisterModule,AuthModule, LoginModule, ImageUploadModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
