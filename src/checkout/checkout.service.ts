import { Injectable } from '@nestjs/common';
import { CreateCheckoutDto } from './dto/create-checkout.dto';
import { UpdateCheckoutDto } from './dto/update-checkout.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Checkout } from './entities/checkout.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CheckoutService {
  constructor(
    @InjectRepository(Checkout)
    public checkoutRepository: Repository<Checkout>
  ){}
  create(createCheckoutDto: CreateCheckoutDto) {
    const checkout=this.checkoutRepository.create(createCheckoutDto);
    return this.checkoutRepository.save(checkout);
  }

  findAll() {
    return this.checkoutRepository.find({relations:{cart:true}});
  }

  findOne(id: number) {
    return `This action returns a #${id} checkout`;
  }

  update(id: number, updateCheckoutDto: UpdateCheckoutDto) {
    return this.checkoutRepository.update(id,updateCheckoutDto);
  }

  remove(id: number) {
    return `This action removes a #${id} checkout`;
  }
}
