
import { ProductCart } from "src/product-cart/entities/product-cart.entity";
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Checkout {
    @PrimaryGeneratedColumn()
    public id:number

    @Column({default:'Pending'})
    public status:string

    @ManyToMany(()=>ProductCart)
    @JoinTable({name:"cart_checkout"})
    cart:ProductCart[]
}