import { ProductCart } from "src/product-cart/entities/product-cart.entity";

export class CreateCheckoutDto {
    public status:string;
    public cart:ProductCart[];
}
