import { Module } from '@nestjs/common';
import { CheckoutService } from './checkout.service';
import { CheckoutController } from './checkout.controller';
import { ProductCartModule } from 'src/product-cart/product-cart.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkout } from './entities/checkout.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Checkout]), ProductCartModule],
  controllers: [CheckoutController],
  providers: [CheckoutService],

})
export class CheckoutModule {}
