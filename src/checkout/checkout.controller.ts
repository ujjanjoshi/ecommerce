import { Controller, Get, Post, Body, Patch, Param, Delete, Request, UseGuards } from '@nestjs/common';
import { CheckoutService } from './checkout.service';
import { CreateCheckoutDto } from './dto/create-checkout.dto';
import { UpdateCheckoutDto } from './dto/update-checkout.dto';
import { AuthGuard } from '@nestjs/passport';
import { ProductCartService } from 'src/product-cart/product-cart.service';

@Controller('checkout')
export class CheckoutController {
  constructor(private readonly checkoutService: CheckoutService,
    private readonly cartService:ProductCartService
    ) {}

  @Post()
  @UseGuards(AuthGuard("jwt"))
  async create(@Request()req:any,@Body() createCheckoutDto: CreateCheckoutDto) {
    const cart=await this.cartService.findOne(req.user.data.id);
    createCheckoutDto.cart=cart;
    this.cartService.updatecheckout(req.user.data.id);
    return this.checkoutService.create(createCheckoutDto);
  }

  @Get()
  findAll() {
    return this.checkoutService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkoutService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCheckoutDto: UpdateCheckoutDto) {
    return this.checkoutService.update(+id, updateCheckoutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkoutService.remove(+id);
  }
}
