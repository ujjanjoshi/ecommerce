import { Controller, Get, Post, Body, Patch, Param, Delete, Request, UseGuards } from '@nestjs/common';
import { ProductCartService } from './product-cart.service';
import { CreateProductCartDto } from './dto/create-product-cart.dto';
import { UpdateProductCartDto } from './dto/update-product-cart.dto';
import { ProductService } from 'src/product/product.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('product-cart')
export class ProductCartController {
  constructor(private readonly productCartService: ProductCartService,
    private readonly productService:ProductService
    ) {}

  @Post(':id/')
  @UseGuards(AuthGuard("jwt"))
  async create(@Param('id')id:number,@Request() req:any) {
    const product= await this.productService.findOne(id);
    return await this.productCartService.create(product[0],req.user.data.id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productCartService.findOne(+id);
  }

  @Get('increasequantity/:id')
  increasequantity(@Param('id') id: string){
    return this.productCartService.increasequantity(+id);
  }

  @Get('decreasequantity/:id')
  decreasequantity(@Param('id') id: string){
    return this.productCartService.decreasequantity(+id);
  }


  // @Patch('checkout/:id')
  // async checkout(@Param('id')id:number){
  // return this.productCartService.checkout(id);
  // }

  // @Patch('processing/:id/:status')
  // async processing(@Param('id')id:string,@Param('status')status:string){
  //   return this.productCartService.processing(+id,status);
  //   }


  // @Get('/processingdetials/:checkout')
  //   async processingdetails(@Param("checkout")checkout:boolean){
  //     return await this.productCartService.details(checkout);
  //   }


    @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productCartService.remove(+id);
  }
}
