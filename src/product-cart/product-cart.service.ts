import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductCartDto } from './dto/create-product-cart.dto';
import { UpdateProductCartDto } from './dto/update-product-cart.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductCart } from './entities/product-cart.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductCartService {
  constructor(
    @InjectRepository(ProductCart)
    public productCartRepository: Repository<ProductCart>
  ){}

  async create(product:Object,userid:number) {
    const createProductCartDto=new CreateProductCartDto();
    createProductCartDto.actualPrice=product['price'];
    createProductCartDto.totalprice= product['price'];
    createProductCartDto.product=product;
    createProductCartDto.userid=userid;
    return this.productCartRepository.save(createProductCartDto);
  }

  async findOne(id: number) {
    return await this.productCartRepository.find({relations:{product:true},where:{userid:id,checkout:false}});
  }

  update(id: number, updateProductCartDto: UpdateProductCartDto) {
    return `This action updates a #${id} productCart`;
  }

  async increasequantity(id:number){
   const cart= await this.productCartRepository.find({relations:{product:true},where:{id:id}});
   if(cart.length==1){
   const quantity=cart[0]['quantity']+1;
   const totalprice= cart[0]['actualPrice']*quantity;
   this.productCartRepository.update(id,{quantity:quantity,totalprice:totalprice})
    return "quantity increased";
  }else{
    throw new NotFoundException();
  }
  }

  async decreasequantity(id:number){
    const cart= await this.productCartRepository.find({relations:{product:true},where:{id:id}});
    if(cart.length==1){
      if(cart[0]['quantity']!=1){
        const quantity=cart[0]['quantity']-1;
        const totalprice= cart[0]['actualPrice']*quantity;
        await this.productCartRepository.update(id,{quantity:quantity,totalprice:totalprice})
      }else{
        this.productCartRepository.delete(id);
      }
      
      return "quantity decreased";
    }else{
      throw new NotFoundException();
    }
   }
  remove(id: number) {
    return this.productCartRepository.delete({userid:id});
  }

  async updatecheckout(id:number){
    return this.productCartRepository.update({userid:id},{checkout:true});
  }
}
