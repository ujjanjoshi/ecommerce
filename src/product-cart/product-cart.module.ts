import { Module } from '@nestjs/common';
import { ProductCartService } from './product-cart.service';
import { ProductCartController } from './product-cart.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductCart } from './entities/product-cart.entity';
import { ProductModule } from 'src/product/product.module';

@Module({
  imports:[TypeOrmModule.forFeature([ProductCart]),ProductModule],
  controllers: [ProductCartController],
  providers: [ProductCartService],
  exports:[ProductCartService]
})
export class ProductCartModule {}
