import { Product } from "src/product/entities/product.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ProductCart {
    @PrimaryGeneratedColumn()
    public id:number;

    @ManyToOne(()=>Product,(product)=>product.cart)
    @JoinColumn()
    product:Product

    @Column()
    public userid:number
    
    @Column({default:1})
    public quantity:number

    @Column({default:false})
    public checkout:boolean;

    @Column()
    public actualPrice:number;

    @Column()
    public totalprice:number;

    @Column({type:"timestamp", default: () => "CURRENT_TIMESTAMP"})
    public createdAt:Date

    @Column({type:"timestamp",default: () => "CURRENT_TIMESTAMP"})
    public updatedAt:Date

}
