export class CreateProductDto {
    public title:string;
    public description:string;
    public quantity:number;
    public price:number;
    public image:Object;
    }
