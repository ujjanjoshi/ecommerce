import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile, UseGuards } from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ImageUploadService } from 'src/image-upload/image-upload.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { AuthGuard } from '@nestjs/passport';
import { RoleGuard } from 'src/role/roleguard/role.guard';
import { CONSTANTS } from 'src/role/constants';

@Controller('product')
export class ProductController {
  constructor(
    private readonly productService: ProductService,
    private readonly ImageUploadService: ImageUploadService
    ) {}

  @Post()
  @UseGuards(AuthGuard("jwt"),new RoleGuard(CONSTANTS.ROLES.ADMIN))
  @UseInterceptors(FileInterceptor('file',{
    storage:diskStorage({
      destination :"./upload/productimage",
      filename:(req,file,cb)=>{
        cb(null,`${file.originalname}`)
      } 
    })
  }))
 async create(@UploadedFile() file: Express.Multer.File,@Body() createProductDto: CreateProductDto) {
  
    const image=await this.ImageUploadService.create(file.originalname)
    return this.productService.create(image,createProductDto);
  }

  @Get()
  findAll() {
    return this.productService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard("jwt"),new RoleGuard(CONSTANTS.ROLES.ADMIN))
  @UseInterceptors(FileInterceptor('file',{
    storage:diskStorage({
      destination :"./upload/productimage",
      filename:(req,file,cb)=>{
        cb(null,`${file.originalname}`)
      } 
    })
  }))
 async update(@Param('id') id: string,@UploadedFile() file: Express.Multer.File, @Body() updateProductDto: UpdateProductDto) {
    if(file!=null){
      const imageid=await this.productService.getProductImageId(+id);
    await this.ImageUploadService.update(imageid,file.originalname);
    }
    return this.productService.update(+id, updateProductDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard("jwt"),new RoleGuard(CONSTANTS.ROLES.ADMIN))
  remove(@Param('id') id: string) {
    return this.productService.remove(+id);
  }
}
