import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    public productRepository:Repository<Product>
  ){}


  create(image:Object,createProductDto:CreateProductDto) {
    createProductDto.image=image;
    const product=this.productRepository.create(createProductDto);
    return  this.productRepository.save(product);
  }

  findAll() {
    return this.productRepository.find({relations:{
      image:true,
    }});
  }

  async findOne(id: number) {
    return await this.productRepository.find({relations:{
      image:true,
    },
  where:{
    id:id
  }});
 
  }
  async getProductImageId(productId: number){
    const product = await this.productRepository.find({relations:{
      image:true,
    },
  where:{
    id:productId
  }});
    return product[0].image.id;
  }
  update(id: number, updateProductDto: UpdateProductDto) {
    
    return this.productRepository.update(id,updateProductDto);
  }

  remove(id: number) {
    return this.productRepository.softDelete(id);
  }
}
