import { ImageUpload } from "src/image-upload/entities/image-upload.entity";
import { ProductCart } from "src/product-cart/entities/product-cart.entity";
import { Column, DeleteDateColumn, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    public id:number;

    @Column()
    public title:string;

    @Column()
    public description:string;

    @Column()
    public quantity:number;

    @Column()
    public price:number;

    @OneToOne(()=>ImageUpload,{ eager: true })
    @JoinColumn()
    public image:ImageUpload;

    @OneToMany(()=>ProductCart,(productcart)=>productcart.product,{cascade:true})
    cart:ProductCart[]

    @Column({type:"timestamp", default: () => "CURRENT_TIMESTAMP"})
    public createdAt:Date

    @Column({type:"timestamp",default: () => "CURRENT_TIMESTAMP"})
    public updatedAt:Date

    @DeleteDateColumn({type:"timestamp"})
    public delete_date:Date;
}
